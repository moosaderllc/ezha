# Ezha-Pygame-Framework

A framework for my Python/Pygame projects

## How to use

Import packages as-needed:

```python
from Ezha import FontManager
from Ezha import LanguageManager
from Ezha import ConfigManager
from Ezha import Button
from Ezha import Image
from Ezha import Label
```

This is a work-in-progress

## Starter project

The [Ezha Template Project](https://github.com/Moosader/Ezha-Template-Project)
has some code all ready to go for you!

## Example code

View [PickinSticks-Ezha](https://github.com/Moosader/PickinSticks-Ezha) for a demo of a basic game.

![Screenshot of Pickin Sticks](https://github.com/Moosader/PickinSticks-Ezha/raw/master/screenshot.png)
