import pygame, sys
from pygame.locals import *

class FontManager:
    fonts = {}
    
    @classmethod
    def Add( pyClass, key, path, size ):
        pyClass.fonts[ key ] = pygame.font.Font( path, size )

    @classmethod
    def Get( pyClass, key ):
        return pyClass.fonts[ key ]

    @classmethod
    def Clear( pyClass ):
        pyClass.fonts.clear()
