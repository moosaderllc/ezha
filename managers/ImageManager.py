import pygame, sys
from pygame.locals import *

class ImageManager:
    images = {}
    
    @classmethod
    def Add( pyClass, key, path ):
        pyClass.images[ key ] = pygame.image.load( path )

    @classmethod
    def Get( pyClass, key ):
        return pyClass.images[ key ]

    @classmethod
    def Clear( pyClass ):
        pyClass.images.clear()
