import pygame, sys, csv
from pygame.locals import *

class LanguageManager:
    gameText = {}
    helperLanguage = None
    targetLanguage = None
    
    @classmethod
    def Setup( pyClass, options ):
        pyClass.gameText = {}
        pyClass.helperLanguage = options["helperLanguage"]
        pyClass.targetLanguage = options["targetLanguage"]
        
        # Load Helper
        filepath = "assets/language/" + options["helperLanguage"] + ".csv"
        
        with open( filepath, "rb" ) as csvfile:
            contents = csv.reader( csvfile, delimiter=',', quotechar='"' )
            
            for row in contents:
                if ( row[0] not in pyClass.gameText ):
                    pyClass.gameText[ row[0] ] = {}
                pyClass.gameText[ row[0] ][ pyClass.helperLanguage ] = row[1]
                
        # Load Target
        filepath = "assets/language/" + options["targetLanguage"] + ".csv"
        
        with open( filepath, "rb" ) as csvfile:
            contents = csv.reader( csvfile, delimiter=',', quotechar='"' )
            
            for row in contents:
                if ( row[0] not in pyClass.gameText ):
                    pyClass.gameText[ row[0] ] = {}
                pyClass.gameText[ row[0] ][ pyClass.targetLanguage ] = row[1]
        
        print( "Helper: ", pyClass.helperLanguage, "Target: ", pyClass.targetLanguage )
        
    
    @classmethod
    def TargetText( pyClass, key ):             
        if ( pyClass.gameText == None or key not in pyClass.gameText ):
            return "NOTFOUND"
        
        return pyClass.gameText[ key ][ pyClass.targetLanguage ]
        
        
    @classmethod
    def HelperText( pyClass, key ):             
        if ( pyClass.gameText == None or key not in pyClass.gameText ):
            return "NOTFOUND"
        
        return pyClass.gameText[ key ][ pyClass.helperLanguage ]

