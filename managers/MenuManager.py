import pygame, sys
from pygame.locals import *

from ezha.ui import Button
from ezha.ui import Image
from ezha.ui import Label
from FontManager import FontManager
from ConfigManager import ConfigManager
from SoundManager import SoundManager

class MenuManager:
    images = {}
    buttonItems = {}
    imageItems = {}
    labelItems = {}
    background = None
    confirmSound = None
    cancelSound = None

    @classmethod
    def SetButtonAudio( pyClass, confirm, cancel ):
        pyClass.confirmSound = confirm
        pyClass.cancelSound = cancel

    @classmethod
    def Load( pyClass, path ):
        menuFile = open( path, "r" )
        contents = menuFile.readlines()
        options = {}
        options.clear()

        readingType = ""

        for line in contents:
            line = line.replace( "\n", "" )
            tokens = line.split()

            if ( len( tokens ) == 0 ):
                continue

            elif ( tokens[0] == "ASSET" ):
                # tokens[1] = key       tokens[2] = path
                pyClass.images[ tokens[1] ] = pygame.image.load( tokens[2] )

            elif ( tokens[0] == "BACKGROUND" ):
                # tokens[1] = key of image
                pyClass.background = pyClass.images[ tokens[1] ]

            elif ( tokens[0] == "END" ):
                ## Save item being loaded
                if ( readingType == "LABEL" ):
                    pyClass.labelItems[ options["key"] ] = Label()
                    pyClass.labelItems[ options["key"] ].Setup( options )

                elif ( readingType == "IMAGE" ):
                    pyClass.imageItems[ options["key"] ] = Image()
                    pyClass.imageItems[ options["key"] ].Setup( options )

                elif ( readingType == "BUTTON" ):
                    pyClass.buttonItems[ options["key"] ] = Button()
                    pyClass.buttonItems[ options["key"] ].Setup( options )

                # Reset the options
                options.clear() #TODO: Actually clear the options dict
                readingType = ""

            elif ( tokens[0] == "LABEL" or tokens[0] == "IMAGE" or tokens[0] == "BUTTON" ):
                readingType = tokens[0]
                options["key"] = tokens[1]

            elif ( tokens[0] == "label_text" ):
                titleString = ""
                for i in range( 1, len( tokens ) ):
                    if ( i != 1 ): titleString += " "
                    titleString += tokens[i]

                options["label_text"] = titleString

            elif ( tokens[0] == "label_text_from_config" ):
                options["label_text"] = ConfigManager.options[ tokens[1] ]

            elif ( tokens[0] == "label_position" ):
                options["label_position"] = ( int( tokens[1] ), int( tokens[2] ) )

            elif ( tokens[0] == "image_position" ):
                options["image_position"] = ( int( tokens[1] ), int( tokens[2] ) )

            elif ( tokens[0] == "button_position" ):
                options["button_position"] = ( int( tokens[1] ), int( tokens[2] ) )
                
            elif ( tokens[0] == "label_color" ):
                options["label_color"] = pygame.Color( int( tokens[1] ), int( tokens[2] ), int( tokens[3] ) )

            elif ( tokens[0] == "label_secondary_color" ):
                options["label_secondary_color"] = pygame.Color( int( tokens[1] ), int( tokens[2] ), int( tokens[3] ) )

            elif ( tokens[0] == "image_blitrect" ):
                options["image_blitrect"] = pygame.Rect( int( tokens[1] ), int( tokens[2] ), int( tokens[3] ), int( tokens[4] ) )
                
            elif ( tokens[0] == "button_blitrect" ):
                options["button_blitrect"] = pygame.Rect( int( tokens[1] ), int( tokens[2] ), int( tokens[3] ), int( tokens[4] ) )

            elif ( tokens[0] == "button_texture" ):
                options["button_texture"] = pyClass.images[ tokens[1] ]
                
            elif ( tokens[0] == "image_texture" ):
                options["image_texture"]  = pyClass.images[ tokens[1] ]

            elif ( tokens[0] == "label_font" ):
                options["label_font"] = FontManager.Get( tokens[1] )
                
            elif ( tokens[0] == "label_style" ):
                options["label_style"] = tokens[1]

            else:
                #  label_style, image_effect, button_action, button_type
                options[ tokens[0] ] = tokens[1]

    @classmethod
    def Clear( pyClass ):
        pyClass.images.clear()
        pyClass.buttonItems.clear()
        pyClass.labelItems.clear()
        pyClass.imageItems.clear()
        pyClass.background = None

    @classmethod
    def Draw( pyClass, windowSurface ):
        windowSurface.blit( pyClass.background, pygame.Rect( 0, 0, 1280, 720 ) )

        for image in pyClass.imageItems:
            pyClass.imageItems[image].Draw( windowSurface )

        for button in pyClass.buttonItems:
            pyClass.buttonItems[button].Draw( windowSurface )

        for label in pyClass.labelItems:
            pyClass.labelItems[label].Draw( windowSurface )

    @classmethod
    def ButtonAction( pyClass, mouseX, mouseY ):
        for button in pyClass.buttonItems:
            if ( pyClass.buttonItems[button].IsClicked( mouseX, mouseY ) ):
                if ( pyClass.buttonItems[button].type == "confirm" ):
                    SoundManager.PlaySound( pyClass.confirmSound )
                else:
                    SoundManager.PlaySound( pyClass.cancelSound )
                    
                return pyClass.buttonItems[button].action

        return ""
