import pygame, sys
from pygame.locals import *

class SoundManager:
    sounds = {}
    music = {}
    soundVolume = 1
    musicVolume = 1

    ##  Add a sound/music clip to the manager
    #   @param  options["isMusic"]  True or False
    #   @param  options["key"]      Identifier to access the audio file with
    #   @param  options["path"]     Path of the audio file to load
    @classmethod
    def Add( pyClass, options ):
        if ( options["isMusic"] ):
            pyClass.music[ options["key"] ] = options["path"]
        else:
            pyClass.sounds[ options["key"] ] = pygame.mixer.Sound( options["path"] )
        
    @classmethod
    def SetMusicVolume( pyClass, amount ):
        if ( amount < 0 ): amount = 0
        elif ( amount > 100 ): amount = 100
        pyClass.musicVolume = amount
        pygame.mixer.music.set_volume( amount )

    @classmethod
    def GetMusicVolume( pyClass ):
        return pyClass.musicVolume
        
    @classmethod
    def SetSoundVolume( pyClass, amount ):
        if ( amount < 0 ): amount = 0
        elif ( amount > 100 ): amount = 100
        pyClass.soundVolume = amount

    @classmethod
    def GetSoundVolume( pyClass ):
        return pyClass.soundVolume

    @classmethod
    def GetSound( pyClass, key ):
        return pyClass.sounds[ key ]
        
    @classmethod
    def GetMusic( pyClass, key ):
        return pyClass.music[ key ]

    @classmethod
    def PlaySound( pyClass, key ):
        if ( pyClass.sounds[ key ] is not None ):
            pyClass.sounds[ key ].set_volume( pyClass.soundVolume )
            pyClass.sounds[ key ].play()
        
    @classmethod
    def PlayMusic( pyClass, key ):
        if ( pyClass.music[ key ] is not None ):
            pygame.mixer.music.load( pyClass.music[ key ] )
            pygame.mixer.music.play()

    @classmethod
    def PauseMusic( pyClass ):
        pygame.mixer.music.pause()

    @classmethod
    def UnpauseMusic( pyClass ):
        pygame.mixer.music.unpause()

    @classmethod
    def Clear( pyClass ):
        pyClass.songs.clear()
        pyClass.sounds.clear()
