#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame, sys
from pygame.locals import *

from Drawable import Drawable

import copy

##  Uses the Drawable object, allows for an animated sprite,
#   where all the frames are stored vertically and horizontally
#   in the sprite sheet.
class Animatable( object ):
    ##  Initialize the Animatable object
    def __init__( self, options ):
        self.drawable = Drawable( {} )
        self.frameSize = pygame.Rect( 0, 0, 32, 48 ) # x and y ignored
        self.maxRows = 4    # Different rows = different actions/directions
        self.maxCols = 4    # Different cols = different frames of animation
        self.currentColumn = 0   # Column index / animation 
        self.currentRow = 0  # Row index / action
        self.animateSpeed = 0.1
        self.drawable.SetFrame( options )

        self.Setup( options )

    ##  Explicitly set up the Animatable object
    def Setup( self, options ):
        self.drawable.Setup( options )
        self.SetSpritesheetDimensions( options )
        self.SetFrameDimensions( options )
        self.SetAction( options )
        self.SetAnimateSpeed( options )

    ##  Set the screen position of the Drawable object
    #   @param  options["posRect"]      The pygame.Rect( x, y, width, height ) of the item to draw. OR
    #   @param  options["x"]            The new x coordinate for the posRect
    #   @param  options["y"]            The new y coordinate for the posRect
    #   width and height correspond to the scaled size on screen.
    def SetPosition( self, options ):        
        self.drawable.SetPosition( options )

    ##  Return the position pygame.Rect( x, y, width, height ) of the drawable object.
    #   width and height correspond to the scaled size on screen.
    def GetPosition( self ):
        return self.drawable.GetPosition()

    ##  Set which portion of the image to display
    #   @param options["frameRect"]     The pygame.Rect ( x, y, width, height ) of the frame.
    #   width and height correspond to the area you're grabbing from the spritesheet.
    def SetFrame( self, options ):
        self.drawable.SetFrame( options )

    ##  Returns the current frame pygame.Rect( x, y, width, height ).
    #   width and height correspond to the area you're grabbing from the spritesheet.
    def GetFrame( self ):
        return self.drawable.GetFrame()

    ##  Set the action (which row) of images to show.
    #   @param  options["row"]  The row (0, 1, 2, not in pixels) to set.
    def SetAction( self, options ):
        if ( "row" in options ):
            self.currentRow = options["row"]

    ##  Returns the current row used. Row will be 0, 1, 2, etc. and not the pixel y coordinate.
    def GetAction( self ):
        return self.currentRow

    ##  Set up dimension information about the spritesheet
    #   @param  options["maxRows"]      Maximum amount of rows (amount of sprites vertically)
    #   @param  options["maxCols"]      Maximum amount of columns (amount of sprites horizontally)
    def SetSpritesheetDimensions( self, options ):
        if ( "maxRows" in options ):
            self.maxRows = options["maxRows"]

        if ( "maxCols" in options ):
            self.maxCols = options["maxCols"]

    ##  Set the dimensions of a single frame
    #   @param  options["width"]        The width of a frame
    #   @param  options["height"]       The height of a frame
    #   @param  options["frameRect"]    The width and height of a frame (x and y ignored)
    def SetFrameDimensions( self, options ):
        if ( "width" in options and "height" in options ):
            self.frameSize.width = options["width"]
            self.frameSize.height = options["height"]

        elif ( "frameRect" in options ):
            self.frameSize = copy.copy( options["frameRect"] )

    ##  Set up the animation speed 
    #   @param  options["animateSpeed"] Animation speed, usually between 0 and 1.
    def SetAnimateSpeed( self, options ):
        if ( "animateSpeed" in options ):
            self.animateSpeed = options["animateSpeed"]

    ##  Increments the frame counter and updates the image
    def Update( self ):
        self.currentColumn += self.animateSpeed
        if ( self.currentColumn >= self.maxCols ):
            self.currentColumn = 0

        frameRect = pygame.Rect( int( self.currentColumn ) * self.frameSize.width, self.currentRow * self.frameSize.height, self.frameSize.width, self.frameSize.height )
        self.drawable.SetFrame( { "frameRect" : frameRect } )


    ##  Draw the image to the screen
    #   @param options.window       The window we're drawing to
    def Draw( self, options ):
        self.drawable.Draw( options )

