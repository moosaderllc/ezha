#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame, sys
from pygame.locals import *

import copy

class VelocityMovable( object ):
    ##  Initialize the VelocityMovable object
    def __init__( self, options ):
        self.velocity = [ 0, 0 ]
        self.acceleration = [ 0, 0 ]
        self.maxAcceleration = 5
        self.maxVelocity = 5
        self.accelerationRate = 1
        self.deaccelerationRate = 0.5   ## This should be less than the acceleration rate
        self.autoAccelerate = [ 0, 0 ]

    ##  Set up the VelocityModule object
    def Setup( self, options ):
        self.SetVelocity( options )
        self.SetAcceleration( options )
        self.SetMaxVelocity( options )
        self.SetMaxAcceleration( options )
        self.SetAccelerationRates( options )
        self.SetAutoAccelerate( options )

    ##  Set whether this item accelerates on its own (like a non-player item)
    #   @param  options["autoAccelerate"]   [ xAmt, yAmt ]
    def SetAutoAccelerate( self, options ):
        if ( "autoAccelerate" in options ):
            self.autoAccelerate = options["autoAccelerate"]

    ##  Set the object's velocity
    #   @param  options["xVelocity"]    The x velocity (positive/negative/0)
    #   @param  options["yVelocity"]    The y velocity (positive/negative/0)
    #   @param  options["xyVelocity"]   The x and y velocity in a list (positive/negative/0)
    def SetVelocity( self, options ):
        if ( "xVelocity" in options ):
            self.velocity[0] = options["xVelocity"]
            
        if ( "yVelocity" in options ):
            self.velocity[1] = options["yVelocity"]

        if ( "xyVelocity" in options ):
            self.velocity[0] = options["xyVelocity"][0]
            self.velocity[1] = options["xyVelocity"][1]

    ##  Return the current [x, y] velocity of the object
    def GetVelocity( self ):
        return self.velocity

    ##  Set the object's acceleration
    #   @param  options["xAcceleration"]    The x acceleration (positive/negative/0)
    #   @param  options["yAcceleration"]    The y acceleration (positive/negative/0)
    #   @param  options["xyAcceleration"]   The x and y acceleration in a list (positive/negative/0)
    def SetAcceleration( self, options ):
        if ( "xAcceleration" in options ):
            self.acceleration[0] = options["xAcceleration"]
            
        if ( "yAcceleration" in options ):
            self.acceleration[1] = options["yAcceleration"]

        if ( "xyAcceleration" in options ):
            self.acceleration[0] = options["xyAcceleration"][0]
            self.acceleration[1] = options["xyAcceleration"][1]

    ##  Return the current [x, y] acceleration of the object
    def GetAcceleration( self ):
        return self.acceleration

    ##  Set the maximum velocity
    #   @param  options["maxVelocity"]
    def SetMaxVelocity( self, options ):
        if ( "maxVelocity" in options ):
            self.maxVelocity = options["maxVelocity"]

    ##  Set the maximum velocity
    #   @param  options["maxAcceleration"]
    def SetMaxAcceleration( self, options ):
        if ( "maxAcceleration" in options ):
            self.maxAcceleration = options["maxAcceleration"]

    ##  Set the maximum velocity
    #   @param  options["accelerationRate"]
    #   @param  options["deaccelerationRate"]
    def SetAccelerationRates( self, options ):
        if ( "accelerationRate" in options ):
            self.accelerationRate = options["accelerationRate"]
            
        if ( "deaccelerationRate" in options ):
            self.deaccelerationRate = options["deaccelerationRate"]

    ##  Allows to add acceleration to the x and y directions
    #   @param  options["xAcceleration"]    -1 (left), 0 (none), or 1 (right)
    #   @param  options["yAcceleration"]    -1 (up), 0 (none), or 1 (down)
    def Accelerate( self, options ):
        if ( "xAcceleration" in options ):
            self.acceleration[0] += options["xAcceleration"] * self.accelerationRate
            
        if ( "yAcceleration" in options ):
            self.acceleration[1] += options["yAcceleration"] * self.accelerationRate

    ##  Update the velocity based on the acceleration. Deaccelerate as well
    def Update( self ):
        # Update acceleration
        self.acceleration[0] += self.autoAccelerate[0]
        self.acceleration[1] += self.autoAccelerate[1]
            
        # Update velocity
        self.velocity[0] += self.acceleration[0]
        self.velocity[1] += self.acceleration[1]

        # Check maximums
        self.KeepInBounds()

        # Degrade acceleration
        if ( self.acceleration[0] < 0 ):
            self.acceleration[0] += self.deaccelerationRate
            
        elif ( self.acceleration[0] > 0 ):
            self.acceleration[0] -= self.deaccelerationRate
            
        if ( self.acceleration[1] < 0 ):
            self.acceleration[1] += self.deaccelerationRate
            
        elif ( self.acceleration[1] > 0 ):
            self.acceleration[1] -= self.deaccelerationRate

        # Degrade velocity
        if ( self.velocity[0] < 0 ):
            self.velocity[0] += self.deaccelerationRate
            
        elif ( self.velocity[0] > 0 ):
            self.velocity[0] -= self.deaccelerationRate
            
        if ( self.velocity[1] < 0 ):
            self.velocity[1] += self.deaccelerationRate
            
        elif ( self.velocity[1] > 0 ):
            self.velocity[1] -= self.deaccelerationRate
            

    ##  Makes sure that the velocity and acceleration don't go out of bounds
    def KeepInBounds( self ):
        # Acceleration
        if ( self.acceleration[0] > self.maxAcceleration ):
            self.acceleration[0] = self.maxAcceleration

        elif ( self.acceleration[0] < -self.maxAcceleration ):
            self.acceleration[0] = -self.maxAcceleration
            
        if ( self.acceleration[1] > self.maxAcceleration ):
            self.acceleration[1] = self.maxAcceleration
            
        elif ( self.acceleration[1] < -self.maxAcceleration ):
            self.acceleration[1] = -self.maxAcceleration

        # Velocity
        if ( self.velocity[0] > self.maxVelocity ):
            self.velocity[0] = self.maxVelocity
            
        elif ( self.velocity[0] < -self.maxVelocity ):
            self.velocity[0] = -self.maxVelocity

        if ( self.velocity[1] > self.maxVelocity ):
            self.velocity[1] = self.maxVelocity
            
        elif ( self.velocity[1] < -self.maxVelocity ):
            self.velocity[1] = -self.maxVelocity
