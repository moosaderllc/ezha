#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame, sys
from pygame.locals import *
import json

from Tile import Tile
from ezha.utilities import Utilities
from ezha.utilities import Logger
from ezha.view import Camera

class Map:
    def __init__( self ):
        self.tilesAbove = []
        self.tilesBelow = []
        self.collisionLayer = []
        self.tileset = None
        self.tileWidth = 0
        self.tileHeight = 0
        self.mapWidth = 0
        self.mapHeight = 0
        self.tilesetDimensionsPx = (0, 0)
        self.objects = []

    def GetObjects( self ):
        return self.objects

    def GetPixelWidth( self ):
        return self.mapWidth * self.tileWidth

    def GetPixelHeight( self ):
        return self.mapHeight * self.tileHeight
        
    def LoadJsonMap( self, path, tileset ):
        # JSON map supports object layer, yay!
        # TODO: Currently only supports 1 tileset
        # TODO: Assumes all tiles are the same size; just gets tile width/height from first layer
        mapFile = open( path, "r" )
        data = json.loads( mapFile.read() )
        self.tileset = tileset

        self.tilesetDimensionsPx = self.tileset.get_size()
        loadingLayer = ""
        loadingMapData = False

        self.mapWidth       = int( data["layers"][0]["width"] )
        self.mapHeight      = int( data["layers"][0]["height"] )
        self.tileWidth      = int( data["tilesets"][0]["tilewidth"] )
        self.tileHeight     = int( data["tilesets"][0]["tileheight"] )
        self.tilesetDimensionsTile = ( self.tilesetDimensionsPx[0] / self.tileWidth, self.tilesetDimensionsPx[1] / self.tileHeight )

        for layer in data["layers"]:

            if ( layer["type"] == "tilelayer" ):                    
                x = 0
                y = 0
                
                for tile in layer["data"]:
                    if ( tile == "0" ):
                        x += 1
                        continue

                    tileX = ( int( tile ) - 1 ) % self.tilesetDimensionsTile[0]
                    tileY = ( int( tile ) - 1 ) / self.tilesetDimensionsTile[0]
                    
                    newTile = Tile()
                    newTile.Setup( { "image" : self.tileset,
                        "posRect" : pygame.Rect( x * self.tileWidth, y * self.tileHeight, self.tileWidth, self.tileHeight ),
                        "frameRect" : pygame.Rect( tileX * self.tileWidth, tileY * self.tileHeight, self.tileWidth, self.tileHeight )
                        } )

                    if ( "above" in loadingLayer.lower() ):
                        self.tilesAbove.append( newTile )
                    elif ( "collision" in loadingLayer.lower() ):
                        self.collisionLayer.append( newTile )
                    else:
                        self.tilesBelow.append( newTile )
                        
                    x += 1
                    if ( x >= self.mapWidth ):
                        x = 0
                        y += 1

            elif ( layer["type"] == "objectgroup" ):
                for obj in layer["objects"]:
                    self.objects.append( obj )
                
        Logger.Write( "Map dimensions:" + str(self.GetPixelWidth()) + " x " + str(self.GetPixelHeight()), "Map::LoadJsonMap" )
        
                
    # TODO: Deprecate this?
    def LoadFlareMap( self, path, tileset ):
        # Flare map doesn't support object layer, as far as I can tell.
        mapFile = open( path, "r" )
        contents = mapFile.readlines()
        self.tileset = tileset

        self.tilesetDimensionsPx = self.tileset.get_size()
        loadingLayer = ""
        loadingMapData = False

        x = 0
        y = 0

        for line in contents:
            line = line.replace( "\n", "" )
            
            if ( "=" in line ):
                tokens = line.split( "=" )
                if   ( tokens[0] == "width" ):      self.mapWidth = int( tokens[1] )
                elif ( tokens[0] == "height" ):     self.mapHeight = int( tokens[1] )
                elif ( tokens[0] == "tilewidth" ):
                    self.tileWidth = int( tokens[1] )
                    if ( self.tileWidth != 0 and self.tileHeight != 0 ):
                        self.tilesetDimensionsTile = ( self.tilesetDimensionsPx[0] / self.tileWidth, self.tilesetDimensionsPx[1] / self.tileHeight )
    
                elif ( tokens[0] == "tileheight" ):
                    self.tileHeight = int( tokens[1] )
                    if ( self.tileWidth != 0 and self.tileHeight != 0 ):
                        self.tilesetDimensionsTile = ( self.tilesetDimensionsPx[0] / self.tileWidth, self.tilesetDimensionsPx[1] / self.tileHeight )
                        
                elif ( tokens[0] == "type" ):
                    loadingLayer = tokens[1]
                    loadingMapData = True

            if ( "tileset=" in line ):
                # Tileset information, e.g. tileset=tileset.png,64,64,0,0
                tileInfo = line.split( "," )    # Actually I don't really need this info.
                    
            elif ( "," in line ):
                # Loading map data
                tiles = line.split( "," )

                # Add each tile to the list
                for tile in tiles:
                    if ( tile == "" or tile == "0" ):
                        x += 1
                        continue
                        
                    newTile = Tile()
                    newTile.Setup( self.tileset, x * self.tileWidth, y * self.tileHeight, self.tileWidth, self.tileHeight )

                    # Tile 23 = tile 2,1 or 128,64
                    tileX = ( int( tile ) - 1 ) % self.tilesetDimensionsTile[0]
                    tileY = ( int( tile ) - 1 ) / self.tilesetDimensionsTile[0]
                    
                    newTile.SetupFrame( tileX * self.tileWidth, tileY * self.tileHeight, self.tileWidth, self.tileHeight )

                    if ( "above" in loadingLayer.lower() ):
                        self.tilesAbove.append( newTile )
                    elif ( "collision" in loadingLayer.lower() ):
                        self.collisionLayer.append( newTile )
                    else:
                        self.tilesBelow.append( newTile )
                        
                    x += 1

                x = 0
                y += 1
                
            elif ( line == "" and loadingMapData == True ):
                # Reset variables to prep for loading a new layer
                loadingMapData = False
                loadingLayer = ""
                loadingTiles = []
                x = 0
                y = 0
                
        Logger.Write( "Map dimensions:", self.GetPixelWidth(), self.GetPixelHeight() )

    def DrawBelow( self, window, camera=None ):
        drawn = 0
        for tile in self.tilesBelow:
            if ( ( camera is None ) or ( camera is not None and camera.DebugIsRegionVisible( tile.GetPosition(), window ) ) ):
                tile.Draw( { "window" : window, "camera" : camera } )
                drawn += 1 

        #Logger.Write( str( drawn ) + "/" + str( len( self.tilesBelow ) ) + " items drawn" )

    def DrawAbove( self, window, camera=None ):
        for tile in self.tilesAbove:
            if ( ( camera is None ) or ( camera is not None and camera.DebugIsRegionVisible( tile.GetPosition(), window ) ) ):
                tile.Draw( { "window" : window, "camera" : camera } )
            

    def Clear( self ):
        del self.tilesAbove[:]
        del self.tilesBelow[:]
        del self.collisionLayer[:]

    def DetectCollision( self, rect ):
        for tile in self.collisionLayer:
            result = Utilities.RectCollision( rect, tile.GetPosition() )
            if ( result == True ):
                return True
